We have the following mailing lists for debian-backports:

* [Debian Backports](https://lists.debian.org/debian-backports/) - For bugreporting and discussions about debian-backports 
* [Debian Backports Announce](https://lists.debian.org/debian-backports-announce/) - (Security) Announces for backports.debian.org
* [Debian Backports Changes](https://lists.debian.org/debian-backports-changes/) - Messages from the backports queued and the Repository (DAK)
